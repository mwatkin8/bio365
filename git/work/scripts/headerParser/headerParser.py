import sys

with open(sys.argv[1]) as file: 
	outfile = open(sys.argv[2], 'w')
	
	seq = ""
	output = ""

	for line in file:
		lineL = []
		newHeader = ""
		headL = []

		if line[0] == ">":
			output += "\n" + seq
			seq = ""
			lineL = line.split("[")
			newHeader = lineL[2]
			headL = newHeader.split(";")
			if newHeader == headL[0]:
				newHeader = "[" + headL[0] + "\n"
			else:
				newHeader = "[" + headL[0] + "]\n"
			seq += newHeader

		else:
			seq += line
			
	outfile.write(output)			
	
  
