#Reconstruct String from Burrows Wheeler Transform (20)
import sys  

def generateColumn(kmers, bwt):
    newKmers = []
    for i in range(0, len(bwt)):
        s = bwt[i] + kmers[i]
        newKmers.append(s)
        newKmers.sort()
        return newKmers


def main(input):
    with open(input,'r') as file:
        outfile = open(sys.argv[2], 'w')
        for line in file:
            bwt = line
            first_col = sorted(bwt, key=str.upper)
            kmers = []
            #Generate the first column of matrix
            for i in range(0, len(bwt)):
                s = bwt[i] + first_col[i]
                kmers.append(s)
            kmers.sort()  
            count = 0  
            while(count != len(bwt) -2):
                kmers = generateColumn(kmers, bwt)
                count += 1

            for kmer in kmers:
		 print kmer
#                if kmer[0] == "$":
 #                   kmer = list(kmer)
  #                  del kmer[0]
   #                 kmer = "".join(kmer)
    #                kmer += "$"
#                    outfile.write(kmer + '\n')

if __name__ == "__main__":
    main(sys.argv[1])

