import sys

with open(sys.argv[1]) as file:
	seq = next(file).strip()
	line = next(file).split(" ")
	k = int(line[0])
	l = int(line[1])
	t = int(line[2])
	outfile = open(sys.argv[2], 'w')
	output = []

	#Create Lmers
	for i in range(0, len(seq) - l):
		lmer = seq[i:(i + l)]
		kmers = {}

		#For each Lmer, make a dictionary of Kmers. [Kmer, #Kmers in Lmer]	
		for j in range(0, l-k+1):
			kmer = lmer[j:(j+k)]
			if kmer not in kmers:
				kmers[kmer] = 1
			else:
				kmers[kmer] = kmers[kmer] + 1
		for kmer in kmers:
			if kmers[kmer] == t:
				if kmer not in output:
					output.append(kmer)		
	for match in output:
		outfile.write(match + " ")	
	outfile.close()	
