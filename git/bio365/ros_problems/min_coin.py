import sys

def main(file_in):
    with open(file_in) as inFile:
        outfile = open(sys.argv[2], 'w')
        money = next(inFile).strip()
        coins = next(inFile).split(',')
        stored_vals = [0]

        for i in range(1, int(money) + 1):
            cur_min_coins = float("inf")
            append = False
            for coin in coins:
                if i - int(coin) >= 0:
                    if stored_vals[i - int(coin)] + 1 < cur_min_coins:
                        if append == True:
                            stored_vals[i] = stored_vals[i - int(coin)] + 1
                        else:
                            stored_vals.append(stored_vals[i-int(coin)] + 1)
                            append = True
                        cur_min_coins = stored_vals[i]     
        outfile.write(str(stored_vals[int(money)]))

if __name__ == '__main__':
    file_in = sys.argv[1]
    main(file_in)
