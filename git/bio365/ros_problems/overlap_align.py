"""
first row and col initialized to 0

if a tie, pick arbitrarily

Suffix of string on the left, start alignment on highest value on the bottom row of the left word, stop when you hit the first col or first row

"""

import sys
import collections

def determineValues(glob, s1, s2, penalty):
    for i in range(1, len(s1)+1): #PAWHEAE
        for j in range(1, len(s2)+1): #HEAGAWGHEE
            #Calculate values
            l = []
            diag, dflag = glob[j-1, i-1]
#            print "s1: " + s1 + "\ti-1: " + str(i-1) + "\ts1[i-1]: " + s1[i-1]
#            print "s2: " + s2 + "\tj-1: " + str(j-1) + "\ts2[j-1]: " + s2[j-1]
            if s1[i-1] == s2[j-1]: #If a match, add the match score +1
                diag = diag + 1
            else:
                diag = diag - penalty #Else, subtract the mismatch penalty (same as indel)
            vert, vflag = glob[j, i-1]
            vert = vert - penalty
            hori, hflag = glob[j-1, i]
            hori = hori - penalty
            
            #Choose the max
            l.append(diag)
            l.append(vert)
            l.append(hori)
            m = max(l)

            #Assign an arrow to the max [0: diag, 1: vert, 2: hori]
            if m == diag:
                glob[j,i] = [m,0]
            if m == vert:
                glob[j,i] = [m,1]
            if m == hori:
                glob[j,i] = [m,2]
#    print collections.OrderedDict(sorted(glob.items()))
    return glob

def getAlignment(glob, s1, s2):
    start = [0,0]
    largest = -100
    for i in range(1, len(s2)+1):
        value, arrow = glob[i, len(s1)]
        if value >= largest:
            largest = value
            start = [i, len(s1)]

    row = start[0]
    col = start[1]

    s2Align = "" #HEAGAWGHEE
    s1Align = "" #PAWHEAE
    maxScore = 0
    
    score, arrow = glob[row, col]
    maxScore = score

    while True:
#        print "row: " + str(row) + "\tcol: " + str(col) 
        score, arrow = glob[row, col]
        if row == 0 or col == 0:
            break
        if score > maxScore:
            maxScore = score 
        if arrow == 0:
            s2Align = s2[row-1] + s2Align
            s1Align = s1[col-1] + s1Align
            row = row-1
            col = col-1
        elif arrow == 1:
            s2Align = "-" + s2Align
            s1Align = s1[col-1] + s1Align
            row = row
            col = col-1 
        elif arrow == 2:
            s2Align = s2[row-1] + s2Align
            s1Align = "-" + s1Align
            row = row-1
            col = col
        
#    print "s1Align: " + s1Align + "\ts2Align: " + s2Align
    return maxScore, s1Align, s2Align

def main(file_in):
    with open(file_in) as inFile:
        outfile = open(sys.argv[2], 'w')
        s1 = next(inFile).strip()
        s2 = next(inFile).strip()
        penalty = int(sys.argv[3])
        glob = {}
        
        #Populate row 0 and column 0 with the penalty
        for i in range(len(s2) + 1):
             glob[i, 0] = [0, 3]
        for i in range(1, len(s1) + 1):
             glob[0, i] = [0, 3]
        #Populate the rest of the grid
        glob = determineValues(glob, s1, s2, penalty)
        #Traverse for mapping - start at highest value of bottom row
        score, s1Align, s2Align = getAlignment(glob, s1, s2)
        outfile.write(str(score) + "\n" + s1Align + "\n" + s2Align)

if __name__ == '__main__':
    file_in = sys.argv[1]
    main(file_in)
