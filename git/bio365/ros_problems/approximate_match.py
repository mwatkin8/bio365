import sys
import itertools

def mutations(word, hamming_distance, charset='ATCG'):
	mismatches = []
	for indices in itertools.combinations( range( len( word ) ), hamming_distance ):
		for replacements in itertools.product(charset, repeat=hamming_distance):
			mutation = list(word)
			for index, replacement in zip( indices, replacements ):
				mutation[ index ] = replacement
				mismatches.append("".join( mutation ))
	mismatchSet = set(mismatches)
	return mismatchSet

with open(sys.argv[1]) as file:

	from datetime import datetime
	startTime = datetime.now()

	pattern = next(file).strip()
	seq = next(file).strip()
	d = int(next(file).strip())
	k = len(pattern)

	outfile = open(sys.argv[2], 'w')
	output = []
	kmers = {}

	mutations = mutations(pattern, d)	
	#Traverse sequence
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]

		for mismatch in mutations:
			if kmer == mismatch:
				output.append(i)

	#Write out the results
	sortedSet = sorted(set(output))
	for match in sortedSet:
		outfile.write(str(match) + " ")	

	print datetime.now() - startTime
	outfile.close()	
