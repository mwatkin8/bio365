import sys

with open(sys.argv[1]) as file:

	pattern = next(file).strip()
	seq = next(file).strip()
        k = len(pattern)
	outfile = open(sys.argv[2], 'w')

	#Traverse sequence
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]
		if kmer == pattern:
			outfile.write(str(i) + " ")	

	outfile.close()	
