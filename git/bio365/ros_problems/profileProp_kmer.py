import sys
import re
from decimal import *

with open(sys.argv[1]) as file:

	seq = next(file).strip()
	k = int(next(file).strip())
	A_Line = next(file).split(" ")
	C_Line = next(file).split(" ")
	G_Line = next(file).split(" ")
	T_Line = next(file).split(" ")
	outfile = open(sys.argv[2], 'w')
	output = []

	width,height = k, 4
	Matrix = [[0 for x in range(width)]for y in range(height)]

	for i in range(0, 4):
		for j in range (0, k):
			if i == 0:
				target = A_Line
			if i == 1:
				target = C_Line
			if i == 2:
				target = G_Line
			if i == 3:
				target = T_Line

			Matrix[i][j] = float(target[j])
	kmers = {}
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]
		prob = 1;
		for j in range(0, len(kmer)):
			if kmer[j] == "A":
				prob = prob * (Matrix[0][j]) 
			if kmer[j] == "C":
				prob = prob * (Matrix[1][j]) 
			if kmer[j] == "G":
				prob = prob * (Matrix[2][j]) 
			if kmer[j] == "T":
				prob = prob * (Matrix[3][j]) 
		kmers[kmer] = prob
	high = 0
	for kmer in kmers:
		if kmers[kmer] > high:
			high = kmers[kmer]

	for kmer in kmers:
		if kmers[kmer] == high:
			output.append(kmer)
	 	
	sortedSet = sorted(set(output))
	for match in sortedSet:
		outfile.write(str(match) + " ")	

	outfile.close()	
