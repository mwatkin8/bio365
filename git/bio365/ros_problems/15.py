import sys

with open(sys.argv[1]) as file:

	outfile = open(sys.argv[2], 'w')
	edges = {}

	for line in file:
		adj = []
		seq = line.strip()
		k = len(seq)
		for i in range(0, len(seq) - k+1):
			edge1 = seq[i:(i+(k-1))]
			edge2 = seq[(i+1):(i+k)]
	
			if edge1 in edges:
				edgeList = list(edges[edge1])
				edgeList.append(edge2)
				edges[edge1] = edgeList
			else:
				adj.append(edge2)
				edges[edge1] = adj
				adj = []
		
	for key in edges:
		adjacents = ""
		listCount = 0
		value = edges[key]
		for i in range(0, len(value)):
			if i != len(value) -1:
				adjacents += value[i] + ", "
			else:
				adjacents += value[i]
		outfile.write(key + "-> " + adjacents + "\n")

	outfile.close()	



"""

For a path, find the 2 unbalanced nodes, create an edge

run eulerian cycle code,
find that edge between those nodes
make one the starting point and one the ending point by rotating the cycle


"""
