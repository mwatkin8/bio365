import sys

with open(sys.argv[1]) as file:

	k = int(next(file).strip())
	seq = next(file).strip()

	outfile = open(sys.argv[2], 'w')

	#Traverse sequence
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]
		outfile.write(kmer + "\n")	

	outfile.close()	
