#Reconstruct String from Burrows Wheeler Transform (20)
import sys  

def generateBWTmatrix(text):
    text = text.strip()
    last = text[-1]
    pattern = last + text[:-1]
    return pattern 

def main(input):
    with open(input,'r') as file:
        outfile = open(sys.argv[2], 'w')
        patternList = []
        for line in file:
            text = line
            pattern = text
            for i in range(len(text)):
                pattern = generateBWTmatrix(pattern)
                patternList.append(pattern)
            sortedPatterns = sorted(patternList)
            bwt = ""
	    for pattern in sortedPatterns:
                bwt += pattern[-1]
            bwt = bwt[1:]
	    outfile.write(bwt)

if __name__ == "__main__":
    main(sys.argv[1])

