import sys
import collections

def buildBlosum62(blos_in):
    with open(blos_in) as blosFile:
        blos = {}
        rowLetters = {}
        colLetters = {}
        first = True
        row = 0
        for line in blosFile:
            line = line.strip()
            lineList = line.split(" ")
            for item in lineList:
                if item == "":
                    lineList.remove(item)
            l = len(lineList)
            for i in range(l):
                if first == True:
                    blos[0,1] = lineList[i]
                    rowLetters[lineList[i]] = [0,1]
                    first = False    
                else:
                    if row == 0:
                        rowLetters[lineList[i]] = [0, i+1]
                    if i == 0 and row != 0:
                        colLetters[lineList[i]] = [row, 0]
                    blos[row,i] = lineList[i]
            row += 1
        return blos, rowLetters, colLetters

def getBlosum62Value(row_letter, col_letter, blos, rowLetters, colLetters):
    Rrow, Rcol = rowLetters[row_letter]
    Crow, Ccol = colLetters[col_letter] 
    b62 = blos[Crow, Rcol]
#    print "row_letter: " + row_letter + " col_letter: " + col_letter + " \t Rrow: " + str(Rrow) + ", Rcol: " + str(Rcol) + " \t Crow: " + str(Crow) + ", Ccol: " + str(Ccol) + "\tb62: " + str(b62)
    return b62
  
def determineValues(glob, blos, s1, s2, penalty, rowLetters, colLetters):
    l1 = len(s1)+1
    l2 = len(s2)+1
    for i in range(1, l1): #PLEASANTLY
        for j in range(1, l2): #MEANLY
            #Calculate values
            diag, dflag = glob[j-1, i-1]
            b62 = getBlosum62Value(s2[j-1], s1[i-1], blos, rowLetters, colLetters)
            diag = diag + int(b62)
            vert, vflag = glob[j, i-1]
            vert = vert - penalty
            hori, hflag = glob[j-1, i]
            hori = hori - penalty
            
            #Choose the max
            m = max(diag, vert, hori)

            #Assign an arrow to the max [0: diag, 1: vert, 2: hori]
            if m == diag:
                glob[j,i] = [m,0]
            elif m == vert:
                glob[j,i] = [m,1]
            elif m == hori:
                glob[j,i] = [m,2]
    return glob

def getAlignment(glob, s1, s2):
    s2Align = "" #MEANLY
    s1Align = "" #PLEASANTLY
    maxScore = 0
    finish = False
    
    row = len(s2)
    col = len(s1)
    score, arrow = glob[row, col]
    maxScore = score

    while finish == False:
#        print "row: " + str(row) + "\tcol: " + str(col) 
        score, arrow = glob[row, col]
#        print "score: " + str(score) + "\tarrow: " + str(arrow)
        if score > maxScore:
            maxScore = score 
        if arrow == 0:
            s2Align = s2[row-1] + s2Align
            s1Align = s1[col-1] + s1Align
            row = row-1
            col = col-1
        elif arrow == 1:
            s2Align = "-" + s2Align
            s1Align = s1[col-1] + s1Align
            row = row
            col = col-1 
        elif arrow == 2:
            s2Align = s2[row-1] + s2Align
            s1Align = "-" + s1Align
            row = row-1
            col = col
        if row == 0 and col == 0:
            finish = True

#    print "s1Align: " + s1Align + "\ts2Align: " + s2Align
    return maxScore, s1Align, s2Align

def main(file_in, blos, rowLetters, colLetters):
    with open(file_in) as inFile:
        outfile = open(sys.argv[2], 'w')
        s1 = next(inFile).strip()
        s2 = next(inFile).strip()
        penalty = int(sys.argv[4])
        glob = {}
        
        #Populate row 0 and column 0 with the penalty
        for i in range(len(s2) + 1):
             glob[i, 0] = [-5 * i, 2]
        for i in range(1, len(s1) + 1):
             glob[0, i] = [-5 * i, 1]
        #Populate the rest of the grid
        glob = determineValues(glob, blos, s1, s2, penalty, rowLetters, colLetters)
        #Traverse for mapping - send bottom right of grid to start  traversal
        score, s1Align, s2Align = getAlignment(glob, s1, s2)
        outfile.write(str(score) + "\n" + s1Align + "\n" + s2Align)

if __name__ == '__main__':
    file_in = sys.argv[1]
    blos_in = sys.argv[3]
    blos, rowLetters, colLetters = buildBlosum62(blos_in)
    main(file_in, blos, rowLetters, colLetters)
