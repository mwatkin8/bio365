import sys
import itertools

def addKmer(mis_kmer, kmers):
	if mis_kmer in kmers:
		kmers[mis_kmer] = kmers[mis_kmer] + 1
	else:
		kmers[mis_kmer] = 1

def mutations(word, hamming_distance, kmers, charset='ATCG'):
	# this enumerates all the positions in word
	#print word
	mismatches = []
	for indices in itertools.combinations( range( len( word ) ), hamming_distance ):
        #print "index:", indices
		for replacements in itertools.product(charset, repeat=hamming_distance):
		#print "\treplacements:", replacements
			mutation = list(word)
			for index, replacement in zip( indices, replacements ):
				#print "\t\t", index, ":", replacement
				mutation[ index ] = replacement
				#print "\t\t\t", mutation
				
				mismatches.append("".join( mutation ))
	for mismatch in set(mismatches):
		addKmer(mismatch, kmers)

with open(sys.argv[1]) as file:
	seq = next(file).strip()
	line = next(file).split(" ")
	k = int(line[0])
	d = int(line[1])
	
	outfile = open(sys.argv[2], 'w')
	output = []
	kmers = {}
	

	#Traverse sequence
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]

		mutations(kmer, d, kmers)
	
	freq = 0
	for kmer in kmers:
		if kmers[kmer] >= freq:
			freq = kmers[kmer]
	for kmer in kmers:
		if kmers[kmer] == freq:
			output.append(kmer)
	#Write the most freq to output.txt
	for match in output:
		outfile.write(match + " ")	
	outfile.close()	
