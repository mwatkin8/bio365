import sys

with open(sys.argv[1]) as file:
	seq = next(file).strip()	
	seqLen = len(seq)
	
	revComp = ""
	
	for i in range(0, seqLen):
		if seq[i] == "C":
			revComp = "G" + revComp
		if seq[i] == "G":
			revComp = "C" + revComp
		if seq[i] == "A":
			revComp = "T" + revComp
		if seq[i] == "T":
			revComp = "A" + revComp
	print revComp
