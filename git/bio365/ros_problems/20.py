import sys  

def main(input):
    with open(input,'r') as file:
        outfile = open(sys.argv[2], 'w')
    
        seq = next(file).strip()
        k = 0
        patterns = []

        for pattern in file:
            patterns.append(pattern.strip())
            k = len(pattern.strip())
        for i in range(0, len(seq) - k+1):
            kmer = seq[i:(i + k)]
            if kmer in patterns:
                outfile.write(str(i) + " ")


if __name__ == "__main__":
    main(sys.argv[1])

