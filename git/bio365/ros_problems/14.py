import sys
import random

def createCycle(edgeDic, newStart):
	first = True
	cycle = []
	while len(edgeDic) > 0:
		if first == True:
			cycle.append(newStart)
			first = False
		if newStart in edgeDic:	
			edges = edgeDic[newStart]
			edge = edges[0]
			if len(edges) > 1:
				edges.remove(edge)
			else:
				del edgeDic[newStart]
			cycle.append(edge)
			newStart = edge
		else:
			return cycle
		if len(edgeDic) == 0:
			return cycle

with open(sys.argv[1]) as file:
	outfile = open(sys.argv[2], 'w')
	edgeDic = {}
	loner_cycles = []
	
	#Create the Dictionary {node, list[edge]}
	for line in file:
		line = line.split("\n")
		nodeList = line[0].split(" -> ")
		edgeList = nodeList[1].split(",")
		edgeDic[nodeList[0]] = edgeList

	#Create the First Cycle
	first = True
	newStart = random.choice(edgeDic.keys())
	cycle = createCycle(edgeDic, newStart)

	while len(edgeDic) > 0:
		#Create subCycle from unvisited edges
		subCycle = []
		newStart = random.choice(edgeDic.keys())
		subCycle = createCycle(edgeDic, newStart)
		newCycle = ""

		#Reorder subCycle to fit into cycle
		for i in range(0, len(subCycle)):
			found = False
			if subCycle[i] in cycle:
				newCycle = "".join(subCycle)
				newCycle = newCycle[i+1:len(newCycle)-1] + newCycle[0:i+1]
				found = True

		if found == True:
			#Insert subCycle into cycle
			for i in range(0,len(cycle)):
				if cycle[i] == newCycle[len(newCycle)-1]:
					cycle[i+1:i+1]=newCycle
					newCycle = ""			
		else:
			#Save it for later
			loner_cycles.append(subCycle)

	while len(loner_cycles) > 0:
		found = False
		for subCycle in loner_cycles:
			for i in range(0, len(subCycle)):
				if subCycle[i] in cycle:
					newCycle = "".join(subCycle)
					newCycle = newCycle[i+1:len(newCycle)-1] + newCycle[0:i+1]
					found = True
					print "___"
					print len(loner_cycles)
					loner_cycles.remove(subCycle)
					print len(loner_cycles)
					print "___"

				if found == True:	
					#Insert subCycle into cycle
					for i in range(0,len(cycle)):			
						if cycle[i] == newCycle[len(newCycle)-1]:
							cycle[i+1:i+1]=newCycle		
							newCycle = ""
							break
					break

	for i in range(0, len(cycle)):
		node = cycle[i]
		if i != len(cycle)-1:
			node = node + "->"
		outfile.write(node)

	outfile.close()	
