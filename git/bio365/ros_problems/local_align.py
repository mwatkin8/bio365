import sys
import collections

def buildPAM250(pam_in):
    with open(pam_in) as pamFile:
        pam = {}
        rowLetters = {}
        colLetters = {}
        first = True
        row = 0
        for line in pamFile:
            line = line.strip()
            lineList = line.split(" ")
            for item in lineList:
                if item == "":
                    lineList.remove(item)
            for i in range(len(lineList)):
                if first == True:
                    pam[0,1] = lineList[i]
                    rowLetters[lineList[i]] = [0,1]
                    first = False    
                else:
                    if row == 0:
                        rowLetters[lineList[i]] = [0, i+1]
                    if i == 0 and row != 0:
                        colLetters[lineList[i]] = [row, 0]
                    pam[row,i] = lineList[i]
            row += 1
        return pam, rowLetters, colLetters

def getPAM250Value(row_letter, col_letter, pam, rowLetters, colLetters):
    Rrow, Rcol = rowLetters[row_letter]
    Crow, Ccol = colLetters[col_letter] 
    pam250 = pam[Crow, Rcol]
#    print "row_letter: " + row_letter + " col_letter: " + col_letter + " \t Rrow: " + str(Rrow) + ", Rcol: " + str(Rcol) + " \t Crow: " + str(Crow) + ", Ccol: " + str(Ccol) + "\tpam250: " + str(pam250)
    return pam250
  
def determineValues(glob, pam, s1, s2, penalty, rowLetters, colLetters):
    for i in range(1, len(s1)+1): #PLEASANTLY
        for j in range(1, len(s2)+1): #MEANLY
            #Calculate values
            l = []
            diag, dflag = glob[j-1, i-1]
            pam250 = getPAM250Value(s2[j-1], s1[i-1], pam, rowLetters, colLetters)
            diag = diag + int(pam250)
            if diag < 0:
                diag = 0
            vert, vflag = glob[j, i-1]
            vert = vert - penalty
            if vert < 0:
                vert = 0
            hori, hflag = glob[j-1, i]
            hori = hori - penalty
            if hori < 0:
                hori = 0    
            
            #Choose the max
            l.append(diag)
            l.append(vert)
            l.append(hori)
            m = max(l)

            #Assign an arrow to the max [0: diag, 1: vert, 2: hori]
            if m == diag:
                glob[j,i] = [m,0]
            if m == vert:
                glob[j,i] = [m,1]
            if m == hori:
                glob[j,i] = [m,2]
    return glob

def getAlignment(glob, s1, s2):
    row, col = max(glob, key=lambda i: glob[i])

    s2Align = "" #MEANLY
    s1Align = "" #PLEASANTLY
    maxScore = 0
    finish = False
    
    score, arrow = glob[row, col]
    maxScore = score

    while finish == False:
#        print "row: " + str(row) + "\tcol: " + str(col) 
        score, arrow = glob[row, col]
        if score == 0:
            break
        if score > maxScore:
            maxScore = score 
        if arrow == 0:
            s2Align = s2[row-1] + s2Align
            s1Align = s1[col-1] + s1Align
            row = row-1
            col = col-1
        elif arrow == 1:
            s2Align = "-" + s2Align
            s1Align = s1[col-1] + s1Align
            row = row
            col = col-1 
        elif arrow == 2:
            s2Align = s2[row-1] + s2Align
            s1Align = "-" + s1Align
            row = row-1
            col = col
        
#    print "s1Align: " + s1Align + "\ts2Align: " + s2Align
    return maxScore, s1Align, s2Align

def main(file_in, blos, rowLetters, colLetters):
    with open(file_in) as inFile:
        outfile = open(sys.argv[2], 'w')
        s1 = next(inFile).strip()
        s2 = next(inFile).strip()
        penalty = int(sys.argv[4])
        glob = {}
        
        #Populate row 0 and column 0 with the penalty
        for i in range(len(s2) + 1):
             glob[i, 0] = [0, 3]
        for i in range(1, len(s1) + 1):
             glob[0, i] = [0, 3]
        #Populate the rest of the grid
        glob = determineValues(glob, blos, s1, s2, penalty, rowLetters, colLetters)
        #Traverse for mapping - send bottom right of grid to start  traversal
        score, s1Align, s2Align = getAlignment(glob, s1, s2)
        outfile.write(str(score) + "\n" + s1Align + "\n" + s2Align)

if __name__ == '__main__':
    file_in = sys.argv[1]
    pam_in = sys.argv[3]
    pam, rowLetters, colLetters = buildPAM250(pam_in)
    main(file_in, pam, rowLetters, colLetters)
