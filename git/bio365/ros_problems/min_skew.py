import sys

with open(sys.argv[1]) as file:
	seq = next(file).strip()

	position = [0]
	for i in range(0, len(seq)):
		if seq[i] == "C":
			position.append(position[-1] - 1)
		if seq[i] == "G":
			position.append(position[-1] + 1)
		if seq[i] == "A":
			position.append(position[-1])
		if seq[i] == "T":
			position.append(position[-1])
	minSkew = 0
	for i in range(0, len(position)):
		if position[i] < minSkew:
			minSkew = position[i]
	
	outfile = open(sys.argv[2], 'w')
	for i in range(0, len(position)):
		if position[i] == minSkew:
			outfile.write(str(i) + " ")

	outfile.close()	

