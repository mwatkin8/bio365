import sy
import itertools

def addKmer(mis_kmer, kmers):
	if mis_kmer in kmers:
		kmers[mis_kmer] = kmers[mis_kmer] + 1
	else:
		kmers[mis_kmer] = 1

def revComp(kmer):
	kmer_list = list(kmer)
	revComp = list(kmer)
	for i in range(0, len(kmer)):
		if kmer_list[-(i + 1)] == "A": 
			revComp[i] = "T"
		if kmer_list[-(i + 1)] == "T":
			revComp[i] = "A"
		if kmer_list[-(i + 1)] == "G":	
			revComp[i] = "C"
		if kmer_list[-(i + 1)] == "C":
			revComp[i] = "G"
	return "".join(revComp)

def mutations(word, hamming_distance, kmers, charset='ATCG'):
	mismatches = []
	for indices in itertools.combinations( range( len( word ) ), hamming_distance ):
		for replacements in itertools.product(charset, repeat=hamming_distance):
			mutation = list(word)
			for index, replacement in zip( indices, replacements ):
				mutation[ index ] = replacement
				mismatches.append("".join( mutation ))
	for mismatch in set(mismatches):
		addKmer(mismatch, kmers)
		addKmer(revComp(mismatch), kmers)

with open(sys.argv[1], 'r') as fh:
	test = ""
	seq = ""	

	for line in fh:
		if line[0] == '>':
			#this is a header line
			pass
		else:
			seq += line.strip()
	
#input = genome
#FIND THE MINIMUM SKEW - gives approximate location of OriC
#output = position(s) in genome of min skew

	position = [0]
	minSkewList = []
	for i in range(0, len(seq)):
		if seq[i] == "C":
			position.append(position[-1] - 1)
		if seq[i] == "G":
			position.append(position[-1] + 1)
		if seq[i] == "A":
			position.append(position[-1])
		if seq[i] == "T":
			position.append(position[-1])
	minSkew = 0
	for i in range(0, len(position)):
		if position[i] < minSkew:
			minSkew = position[i]
	
	for i in range(0, len(position)):
		if position[i] == minSkew:
			minSkewList.append(str(i))
	print minSkew
	print minSkewList

#input = 500 window for each min skew position
#FREQUENT WORDS & HAMMING DISTANCE - 9-mers (and reverse compliments) that appear 3 or more times with a certain hamming distance accounted for
#output = any 9-mers repeated 3 or more times

	k = 9 #9-Kmer is typical DNAa Box Length
	d = 1 #1 is the highest acceptable Hamming Distance we will allow
	output = []
	kmers = {}

	#Create 500 window at each minSkew position
	for position in minSkewList:
		
		skewWindow = seq[position : position + 499]

		for i in range(0, len(seq) - k+1):
			kmer = seq[i:(i + k)]
			if d > 0:
				mutations(kmer, d, kmers)
			else:
				addKmer(kmer, kmers)
				addKmer(revComp(kmer), kmers)
	
		freq = 0
		for kmer in kmers:
			if kmers[kmer] >= freq:
				freq = kmers[kmer]
		for kmer in kmers:
			if kmers[kmer] == freq:
				output.append(kmer)
	
		print output


#input = 9-mers repeated 3 or more times with hamming distance accounted for
#FIND POSITIONS OF APPROXIMATE PATTERNS
#output = positions of patterns close to our most freq patterns found


	pattern = next(file).strip()
	seq = next(file).strip()
	d = int(next(file).strip())
	k = len(pattern)

	outfile = open(sys.argv[2], 'w')
	output = []
	kmers = {}

	mutations = mutations(pattern, d)	
	#Traverse sequence
	for i in range(0, len(seq) - k+1):
		kmer = seq[i:(i + k)]

		for mismatch in mutations:
			if kmer == mismatch:
				output.append(i)

	#Write out the results
	sortedSet = sorted(set(output))
	for match in sortedSet:
		outfile.write(str(match) + " ")	

	print datetime.now() - startTime
	outfile.close()	



#Look up argparse



